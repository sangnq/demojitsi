import React, {useEffect, useState} from 'react';
import JitsiMeet, {JitsiMeetView} from 'react-native-jitsi-meet';
import {View, Text, Button} from 'react-native';
import {fetchCreateCall} from './config/api';
import Login from './component/Login';

function App() {
  const [listBenhNhan, setListBenhNhan] = useState(null);
  const [token, setToken] = useState(null);

  useEffect(() => {}, []);

  useEffect(() => {
    return () => {
      JitsiMeet.endCall();
    };
  });

  function onConferenceTerminated(nativeEvent) {
    console.log(nativeEvent);
  }

  function onConferenceJoined(nativeEvent) {
    console.log(nativeEvent);
  }

  function onConferenceWillJoin(nativeEvent) {
    console.log(nativeEvent);
  }

  const startCall = (data) => {
    const url = `https://jitsi-meet.vnptit3.vn/${data.phienCuocGoi}?jwt=${data.token}`;
    console.log('url', url);
    const userInfo = {
      displayName: 'User',
      // email: 'user@example.com',
      // avatar: 'https:/gravatar.com/avatar/abc123',
    };
    JitsiMeet.call(url, userInfo);
  };

  const handleListBenhNhan = (data) => {
    console.log(data);
    setListBenhNhan(data);
  };

  const handleCreateCall = async (data) => {
    const res = await fetchCreateCall(
      {
        idDangKy: data.MA_YC,
        maCoSoYTe: data.MA_CSYT,
        maNguoiNhan: data.USER_NAME,
      },
      token,
    );
    console.log(res);
    startCall(res.data.object);
  };

  return (
    <View>
      {!listBenhNhan && (
        <Login
          getToken={(token) => setToken(token)}
          handleListBenhNhan={handleListBenhNhan}
        />
      )}

      {listBenhNhan &&
        listBenhNhan.map((data) => {
          return (
            <View
              key={data.MA_YC}
              style={{
                flexDirection: 'row',
                margin: 10,
                borderColor: 'gray',
                borderBottomWidth: 1,
              }}>
              <Text
                style={{
                  alignSelf: 'center',
                  paddingRight: 50,
                  fontWeight: 'bold',
                }}>
                {data.TEN_BENHNHAN}
              </Text>
              <View style={{flex: 2, justifyContent: 'space-between'}}>
                <Button
                  style={{margin: 10, width: 100}}
                  onPress={() => {
                    handleCreateCall(data);
                  }}
                  title="Gọi"
                  color="green"
                />
              </View>
            </View>
          );
        })}

      {listBenhNhan && (
        <JitsiMeetView
          onConferenceTerminated={(e) => onConferenceTerminated(e)}
          onConferenceJoined={(e) => onConferenceJoined(e)}
          onConferenceWillJoin={(e) => onConferenceWillJoin(e)}
          style={{
            height: '80%',
            width: '100%',
          }}
        />
      )}
    </View>
  );
}
export default App;
