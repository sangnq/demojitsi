import React, {useEffect} from 'react';
import JitsiMeet, {JitsiMeetView} from 'react-native-jitsi-meet';
import axios from 'axios';

const config = () => {
  axios.defaults.headers['Token-id'] = '97388db0-6ce9-11ea-bc55-0242ac130003';
  axios.defaults.headers['Mac-address'] = 'WEB';
  axios.defaults.headers['Content-Type'] = 'application/json';
};

async function fetchLogin(data) {
  console.log(data);
  config();
  let res;
  const response = await axios.post(
    'https://apigateway-ncovi.vnptit3.vn/auth-service/oauth/token',
    {
      username: data.username,
      password: data.password,
      client_id: 'clientapp',
      client_secret: 'password',
      grant_type: 'password',
    },
  );
  console.log(response);
  return response;
}

async function fetchListYeuCauKham(data) {
  console.log(data);
  config();
  axios.defaults.headers.common['Authorization'] =
    'Bearer ' + data.access_token;
  const res = await axios.get(
    'https://apigateway-ncovi.vnptit3.vn/business-service/api/v2/Manual/list-yeu-cau-kham',
    {
      params: {
        tuNgay: '2020-04-01',
        denNgay: '2020-05-25',
        benhVienId: 10873,
        trangThai: 4,
      },
    },
  );
  return res;
}

async function fetchCreateCall(data, token) {
  console.log(data);
  console.log(token);
  config();
  axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
  let res;
  const response = await axios.post(
    'https://apigateway-ncovi.vnptit3.vn/doctor-service/api/create-call',
    {
      idDangKy: data.idDangKy,
      maCoSoYTe: data.maCoSoYTe,
      maNguoiGoi: 'sonlx.dng@vnpt.vn',
      maNguoiNhan: data.maNguoiNhan,
      tenNguoiGoi: 'Lê Xuân Sơn',
    },
  );
  console.log(response);
  return response;
}

export {fetchLogin, fetchCreateCall, fetchListYeuCauKham};
