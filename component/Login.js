import React, {useEffect, useState} from 'react';
import {View, Button, ScrollView, TextInput, Text} from 'react-native';
import {fetchLogin, fetchListYeuCauKham} from '../config/api';

function Login(props) {
  const [username, setUsername] = useState('sonlx.dng@vnpt.vn');
  const [password, setPassword] = useState('Abc@1234321');

  const handleLogin = async () => {
    const res = await fetchLogin({username, password});
    const list = await fetchListYeuCauKham(res.data);
    props.handleListBenhNhan(list.data.object.data);
    props.getToken(res.data.access_token);
    // console.log('res', list.data.object.data);
  };

  return (
    <View>
      <View style={{flexDirection: 'column', margin: 5}}>
        <Text style={{margin: 5}}>Email: </Text>
        <TextInput
          style={{
            height: 40,
            borderColor: 'gray',
            borderWidth: 1,
          }}
          onChangeText={(text) => setUsername(text)}
          disabled
          value="sonlx.dng@vnpt.vn"
        />
      </View>
      <View style={{flexDirection: 'column', margin: 5}}>
        <Text style={{margin: 5}}>Password: </Text>
        <TextInput
          style={{
            height: 40,
            borderColor: 'gray',
            borderWidth: 1,
          }}
          disabled
          secureTextEntry={true}
          onChangeText={(text) => setPassword(text)}
          value="Abc@1234321"
        />
      </View>
      <Button
        onPress={() => {
          handleLogin();
        }}
        title="Login"
        color="green"
      />
    </View>
  );
}
export default Login;
